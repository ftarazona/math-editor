# Math Editor

This project is an editor which evaluated mathematical expressions in runtime.

## Structure

The project divides in two main parts :

### Mathematical parsing and evaluation

This part aims at building tools for describing mathematical expressions in order to handle them in parsing, evaluating, derivating, etc.

This is also an occasion to learn about the structures and algorithms used for parsing, such as ASTs, graphs, regex...

### Editing

This part aims at giving the user an interface for writing, reading, editing documents. The basic interaction with the terminal will be handled by an external crate, such that this parts mainly deals with user experience.

## Author

Originally this project is written in Rust by Florian Tarazona, 2nd year student at Télécom Paris.
