//! editor is a submodule of math_editor
//!
//! It provides an editor that integrates mathematical expressions

use crossterm::cursor::MoveTo;
use crossterm::event::{read, Event, KeyCode};
use crossterm::execute;
use crossterm::style::{Color, Print, SetForegroundColor};
use crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, size, Clear, ClearType, EnterAlternateScreen,
    LeaveAlternateScreen,
};
use std::io::stdout;

use super::math::math_core::MathCore;

/// The editor works with an effective position, i.e. the position displayed on screen.
/// As the text is divided into fragments, the real position (what fragment, where in this
/// fragment) is needed when modifying the text.
/// Position parameterized enumeration indicates whether the effective position corresponds to a
/// text or a math fragment, what fragment and where in this fragment.
#[derive(Clone, Copy)]
enum Position {
    TextFrag(usize, usize),
    MathFrag(usize, usize),
}

/// When searching what fragment we are in, some conditions may be different may we want to insert
/// or remove a character.
/// PositionMode enumeration is used for indicating the algorithm what we are looking for.
enum PositionMode {
    InsertMode,
    RemoveMode,
}

/// An editor remembers the text already typed and make calculations when exiting a mathematical
/// expression.
pub struct Editor {
    text_frags: Vec<String>,
    math_frags: Vec<(String, Option<String>)>,
    pos: usize,

    math_core: MathCore,
}

/// When dropping an editor we must be assured that the terminal is left without side effects such
/// the raw mode enabled.
impl Drop for Editor {
    fn drop(&mut self) {
        let _ = execute!(stdout(), LeaveAlternateScreen);
        let _ = disable_raw_mode();
    }
}

impl Editor {
    /// new method enters an editor. Some preparation of the terminal is needed before letting the
    /// user type. We enter raw mode for more liberty. We also switch to an alternate screen which
    /// will allow us to find back the terminal when finished. Finally we make sure the alternate
    /// screen is cleared.
    pub fn new() -> crossterm::Result<Self> {
        enable_raw_mode()?;
        execute!(
            stdout(),
            EnterAlternateScreen,
            MoveTo(0, 0),
            Clear(ClearType::All)
        )?;
        Ok(Editor {
            text_frags: vec![String::new()],
            math_frags: Vec::new(),
            pos: 0,
            math_core: MathCore::new(),
        })
    }

    /// len returns the size of the current text. As it is divided into fragments, the operation is
    /// non trivial.
    fn len(&self) -> usize {
        self.text_frags
            .iter()
            .fold(0, |acc, s| acc + s.chars().count())
            + self.math_frags.iter().fold(0, |acc, (expr_str, eval_str)| {
                acc + match eval_str {
                    Some(eval_str) => eval_str.chars().count(),
                    None => expr_str.chars().count() + 2,
                }
            })
    }

    /// display writes the text onto the screen after having cleared it. It displays all the
    /// fragments one by one.
    fn display(&self) -> crossterm::Result<()> {
        let text_frags = self.text_frags.iter();
        let math_frags = self
            .math_frags
            .iter()
            .map(|(expr_str, eval_str)| match eval_str {
                Some(eval_str) => eval_str.to_string(),
                None => format!("`{}`", expr_str),
            });

        execute!(stdout(), Clear(ClearType::All), MoveTo(0, 0))?;

        for (text_frag, math_frag) in text_frags.zip(math_frags) {
            execute!(stdout(), SetForegroundColor(Color::Black), Print(text_frag))?;
            execute!(stdout(), SetForegroundColor(Color::Blue), Print(math_frag))?;
        }
        execute!(
            stdout(),
            SetForegroundColor(Color::Black),
            Print(self.text_frags.iter().last().expect("Malformed fragments"))
        )
    }

    /// As the text is divided into fragments, the "effective" position, visible to the user lacks
    /// information about which fragment we are working on.
    /// get_real_position returns this piece of information, being indicated whether we want it for
    /// inserting or removing.
    fn get_real_position(&self, pos: isize, mode: PositionMode) -> Option<Position> {
        if pos < 0 {
            return None;
        }

        let pos = pos as usize;
        let mut acc = 0;

        // We turn the information of the text of the fragments within the information of the
        // length corresponding, which is the only relevant for the algorithm.
        let mut text_frags = self
            .text_frags
            .iter()
            .map(|s| s.chars().count())
            .enumerate();
        let mut math_frags = self
            .math_frags
            .iter()
            .map(|(expr_str, eval_str)| match eval_str {
                Some(eval_str) => eval_str.chars().count(),
                None => expr_str.chars().count() + 2,
            })
            .enumerate();

        let insert = if let PositionMode::InsertMode = mode {
            true
        } else {
            false
        };

        loop {
            if let Some((i, len)) = text_frags.next() {
                //When inserting at the beginning of a math fragment, we actually want to insert at
                //the end of the preceding text fragment.
                //When removing, we want to delete the fragment itself, which is handled by remove,
                //but we need to inform it that the math fragment is concerned and not the text
                //fragment.
                if (insert && pos <= acc + len) || (!insert && pos < acc + len) {
                    break Some(Position::TextFrag(i, pos - acc));
                } else {
                    acc += len;
                }
            } else {
                break None;
            }
            if let Some((i, len)) = math_frags.next() {
                //Conveniently, the handling of deleting at the end of an mathematical expression
                //and the '`' supressing handling makes it useless to distinguish insert/remove
                //modes.
                if pos < acc + len  {
                    break Some(Position::MathFrag(i, pos - acc));
                } else {
                    acc += len;
                }
            } else {
                break None;
            }
        }
    }

    /// insert handles the adding of a character within the text.
    fn insert(&mut self, c: char) {
        match (
            c,
            self.get_real_position(self.pos as isize, PositionMode::InsertMode),
        ) {
            ('`', Some(Position::TextFrag(i, pos))) => {
                let new_frag = self.text_frags[i].split_off(pos);
                self.text_frags.insert(i + 1, new_frag);
                self.math_frags.insert(i, (String::new(), None));
                self.pos += 1;
            }
            ('`', Some(Position::MathFrag(i, pos))) =>  {
                if pos == self.math_frags[i].0.len() + 1    {
                    self.pos += 1;
                }
            }
            (_, Some(Position::TextFrag(i, pos))) => {
                self.text_frags[i].insert(pos, c);
                self.pos += 1;
            }
            (_, Some(Position::MathFrag(i, pos))) => {
                if pos > 0 {
                    if self.math_frags[i].1.is_none() {
                        self.math_frags[i].0.insert(pos - 1, c);
                    }
                } else {
                    self.text_frags[i].push(c);
                }
                self.pos += 1;
            }
            _ => {}
        }
    }

    /// remove handles the deletion of a character within the text.
    fn remove(&mut self) {
        match self.get_real_position(self.pos as isize - 1, PositionMode::RemoveMode) {
            Some(Position::TextFrag(i, pos)) => {
                if pos == self.text_frags[i].len() {
                    self.text_frags[i].pop();
                } else {
                    self.text_frags[i].remove(pos);
                }
            }
            Some(Position::MathFrag(i, pos)) => {
                if pos > 0 && pos <= self.math_frags[i].0.len() && self.math_frags[i].1.is_none() {
                    let pos = pos - 1;
                    if pos == self.math_frags[i].0.len() {
                        self.math_frags[i].0.pop();
                    } else {
                        self.math_frags[i].0.remove(pos);
                    }
                } else {
                    self.math_frags.remove(i);
                    let to_merge = self.text_frags.remove(i + 1);
                    self.text_frags[i].push_str(to_merge.as_str());
                }
            }
            None => {}
        }
        self.pos = self.pos.saturating_sub(1);
    }

    /// When the cursor enters a mathematical expression, we need to suppose it is not evaluated
    /// anymore.
    fn on_enter_math_frag(&mut self, i: usize)   {
        self.math_frags[i].1 = None;
    }

    /// When the cursor exits a mathematical expression, we try to evaluate it.
    fn on_exit_math_frag(&mut self, j: usize)   {
        if self.math_frags.get(j).is_some() {
            let id = self.math_core.add(self.math_frags[j].0.as_str());
            let eval_str = self.math_core.get_string(id).unwrap();
            self.math_frags[j].1 = Some(eval_str);
        }
    }

    /// edit handles the main loop, which consists in some checks for the cursor, displaying
    /// current text, entering/exiting expressions if needed, then catching characters and special
    /// characters typed.
    pub fn edit(&mut self) -> crossterm::Result<()> {
        let mut old_position = None;
        loop {
            let (cols, _) = size()?;
            if self.pos > self.len() {
                self.pos = self.len();
            }
            let position = self.get_real_position(self.pos as isize, PositionMode::RemoveMode);
            match (old_position, position) {
                (Some(Position::MathFrag(i, _)), Some(Position::MathFrag(j, _))) => {
                    if i != j {
                        self.on_exit_math_frag(i);
                        self.on_enter_math_frag(j);
                    }
                }
                (_, Some(Position::MathFrag(i, _))) => {
                    self.on_enter_math_frag(i);
                }
                (Some(Position::MathFrag(j, _)), _) => {
                    self.on_exit_math_frag(j);
                }
                _ => {}
            }
            old_position = position;
            self.display()?;
            if self.pos > self.len() {
                self.pos = self.len();
            }
            execute!(
                stdout(),
                MoveTo(self.pos as u16 % cols, self.pos as u16 / cols)
            )?;
            match read()? {
                Event::Key(event) => match event.code {
                    KeyCode::Esc => break,
                    KeyCode::Char(c) => {
                        self.insert(c);
                    }
                    KeyCode::Backspace => {
                        self.remove();
                    }
                    KeyCode::Up => {
                        self.pos = self.pos.saturating_sub(cols as usize);
                    }
                    KeyCode::Down => {
                        self.pos += cols as usize;
                    }
                    KeyCode::Left => self.pos = self.pos.saturating_sub(1),
                    KeyCode::Right => self.pos += 1,
                    _ => {}
                },
                _ => {}
            };
        }
        Ok(())
    }
}
