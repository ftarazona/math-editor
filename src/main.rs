use math_editor::editor::Editor;

fn main() {
    let mut editor = Editor::new().unwrap();
    editor.edit().unwrap();
}
