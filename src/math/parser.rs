//! Submodule parser is part of submodule math
//!
//! It wraps its submodules into a more easy-to-use function

pub mod expression;
pub mod expression_parser;
pub mod lexer;

use expression::Expression;
use expression_parser::{parse_expression, ParserError};
use lexer::tokenize;

pub fn parse(s: &str) -> Result<Expression, ParserError> {
    if let Ok(toks) = tokenize(s) {
        if let Some(e) = parse_expression(&mut toks.iter().peekable())? {
            Ok(e)
        } else {
            Err(ParserError::NoExpressionFound)
        }
    } else {
        Err(ParserError::TokenizationError)
    }
}

#[cfg(test)]
mod tests {
    use super::expression::{FuncTab, VarTab};
    use super::*;
    use log::info;

    fn init_logger() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn parsing_some_expressions() {
        init_logger();
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let expressions = [
            ("x = 3.0", 3.0),
            ("y = x * x * x", 27.0),
            ("(y - x * x) / 2", 9.0),
        ];
        for (e, r) in expressions.iter() {
            info!("Evaluating {}, result expected : {}", e, r);
            assert_eq!(parse(e).unwrap().eval(&mut vars, &mut funcs).unwrap(), *r);
        }
    }
}
