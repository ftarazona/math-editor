//! math_core submodule is part of math submodule
//!
//! Evaluation of mathematical expressions requires a context, e.g. variables or function
//! definitions. This context may be changed when evaluating assignment expressions.
//! Furthermore, we may want to reevaluate expressions.
//!
//! math_core provides a so-called context and allows modifications

use std::collections::HashMap;

use super::parser::expression::*;
use super::parser::expression_parser::*;
use super::parser::*;

type ExprTab = HashMap<usize, AdvancedExpression>;

#[derive(Debug)]
pub enum MathCoreError {
    ParserError(ParserError),
    EvaluationError(EvaluationError),
    IDAlreadyRemoved,
    IDNotAssigned,
}

struct AdvancedExpression {
    expr_str: String,
    expr: Option<Expression>,
    evaluation: Option<f32>,
}

impl ToString for AdvancedExpression {
    fn to_string(&self) -> String {
        match self.evaluation {
            Some(n) => n.to_string(),
            None => self.expr_str.to_string(),
        }
    }
}

impl AdvancedExpression {
    fn new() -> Self {
        AdvancedExpression {
            expr_str: String::new(),
            expr: None,
            evaluation: None,
        }
    }

    fn update(&mut self, expr_str: String) {
        self.expr_str = expr_str;
        self.expr = None;
        self.evaluation = None;
    }

    fn try_evaluate(
        &mut self,
        vars: &mut VarTab,
        funcs: &mut FuncTab,
    ) -> Result<f32, MathCoreError> {
        match parse(&self.expr_str) {
            Ok(expr) => {
                let ret = match expr.eval(vars, funcs) {
                    Ok(n) => {
                        self.evaluation = Some(n);
                        Ok(n)
                    }
                    Err(e) => Err(MathCoreError::EvaluationError(e)),
                };
                self.expr = Some(expr);
                ret
            }
            Err(e) => Err(MathCoreError::ParserError(e)),
        }
    }
}

pub struct MathCore {
    vars: VarTab,
    funcs: FuncTab,
    exprs: ExprTab,
    current_id: usize,
}

impl MathCore {
    pub fn new() -> Self {
        MathCore {
            vars: VarTab::new(),
            funcs: FuncTab::new(),
            exprs: ExprTab::new(),
            current_id: 0,
        }
    }

    fn get(&self, id: usize) -> Result<&AdvancedExpression, MathCoreError> {
        if id >= self.current_id {
            Err(MathCoreError::IDNotAssigned)
        } else {
            if let Some(expr) = self.exprs.get(&id) {
                Ok(expr)
            } else {
                Err(MathCoreError::IDAlreadyRemoved)
            }
        }
    }

    pub fn add(&mut self, s: &str) -> usize {
        let mut expr = AdvancedExpression::new();
        if !s.is_empty() {
            expr.update(s.to_string());
            let _ = expr.try_evaluate(&mut self.vars, &mut self.funcs);
        }
        self.exprs.insert(self.current_id, expr);
        self.current_id += 1;
        self.current_id - 1
    }

    pub fn update(&mut self, id: usize, s: String) -> Result<(), MathCoreError> {
        if let Some(expr) = self.exprs.get_mut(&id) {
            expr.update(s);
            expr.try_evaluate(&mut self.vars, &mut self.funcs)?;
            Ok(())
        } else {
            Err(MathCoreError::IDNotAssigned)
        }
    }

    pub fn remove(&mut self, id: usize) {
        self.exprs.remove(&id);
        self.vars = VarTab::new();
        self.funcs = FuncTab::new();
        for (_, expr) in self.exprs.iter_mut() {
            let _ = expr.try_evaluate(&mut self.vars, &mut self.funcs);
        }
    }

    pub fn get_string(&self, id: usize) -> Result<String, MathCoreError> {
        Ok(self.get(id)?.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn init_logger() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn math_core_add_expressions() {
        init_logger();
        let mut core = MathCore::new();
        let _id1 = core.add("x = 2");
        let _id2 = core.add("y = 4");
        let _id3 = core.add("x = 3");
        let id4 = core.add("x + y");
        //assert_eq!(core.get_value(id4).unwrap(), 7.0);
    }

    //    #[test]
    #[should_panic]
    fn math_core_removal() {
        init_logger();
        let mut core = MathCore::new();
        let id1 = core.add("x = 2");
        let _ = core.remove(id1);
        let id2 = core.add("x");
        //core.get_value(id2).unwrap();
    }
}
