//! Submodule lexer is part of parser submodule
//!
//! In order to parse an expression, we must first recognize the tokens in it.
//!
//! This submodule provides tools for building a list of tokens given an expression. The language
//! used is a quite natural mathematical one.

pub mod graph;
pub mod lexer_graph;

use log::debug;

use graph::*;
use lexer_graph::*;

/// A token represents a word in our grammar.
#[derive(PartialEq, Debug)]
pub enum Token {
    EOS, //End of string
    OpeningParenthesis,
    ClosingParenthesis,
    Literal(f32),
    Addition,
    Substraction,
    Product,
    Division,
    SymbolName(String),
    Assignment,
}

/// The lexer can fail to recognize a token in a given expression
#[derive(Debug)]
pub enum LexerError {
    UnrecognizedToken(String),
    CouldNotParseLiteral(String),
}

/// Returns the list of tokens found examining a given expression from left to right
pub fn tokenize(s: &str) -> Result<Vec<Token>, LexerError> {
    let s = s.replace(" ", "");
    let mut s = s.as_str();
    let mut tokens = Vec::new();
    while !s.is_empty() {
        debug!("Remaining : {}", s);
        let graph_result = accept(s, 0, &GRAPH, &ACCEPTING);
        match graph_result {
            Ok((state, string, remaining)) => {
                tokens.push(match state {
                    1 => Token::Addition,
                    2 => Token::Substraction,
                    3 => Token::Product,
                    4 => Token::Division,
                    5 => Token::SymbolName(string),
                    6 => Token::OpeningParenthesis,
                    7 => Token::ClosingParenthesis,
                    8 => {
                        if let Ok(n) = string.trim().parse::<f32>() {
                            Token::Literal(n)
                        } else {
                            return Err(LexerError::CouldNotParseLiteral(string));
                        }
                    }
                    10 => Token::Assignment,
                    _ => unreachable!(),
                });
                s = remaining;
            }
            Err(_) => return Err(LexerError::UnrecognizedToken(s.to_string())),
        }
    }
    tokens.push(Token::EOS);
    Ok(tokens)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn init_logger() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn tokens_of_empty_string() {
        init_logger();
        assert_eq!(tokenize("").unwrap(), [Token::EOS]);
    }

    #[test]
    fn tokens_of_simple_expressions() {
        init_logger();
        assert_eq!(
            tokenize("3+4").unwrap(),
            [
                Token::Literal(3.0),
                Token::Addition,
                Token::Literal(4.0),
                Token::EOS
            ]
        );
        assert_eq!(
            tokenize("(3*4)").unwrap(),
            [
                Token::OpeningParenthesis,
                Token::Literal(3.0),
                Token::Product,
                Token::Literal(4.0),
                Token::ClosingParenthesis,
                Token::EOS
            ]
        );
        assert_eq!(
            tokenize("-(3)").unwrap(),
            [
                Token::Substraction,
                Token::OpeningParenthesis,
                Token::Literal(3.0),
                Token::ClosingParenthesis,
                Token::EOS
            ]
        );
        assert_eq!(
            tokenize("variable*4").unwrap(),
            [
                Token::SymbolName("variable".to_string()),
                Token::Product,
                Token::Literal(4.0),
                Token::EOS
            ]
        );
    }

    #[test]
    fn tokens_of_complex_expression() {
        init_logger();
        assert_eq!(
            tokenize("u_n((3+2)*(-1)+n)").unwrap(),
            [
                Token::SymbolName("u_n".to_string()),
                Token::OpeningParenthesis,
                Token::OpeningParenthesis,
                Token::Literal(3.0),
                Token::Addition,
                Token::Literal(2.0),
                Token::ClosingParenthesis,
                Token::Product,
                Token::OpeningParenthesis,
                Token::Substraction,
                Token::Literal(1.0),
                Token::ClosingParenthesis,
                Token::Addition,
                Token::SymbolName("n".to_string()),
                Token::ClosingParenthesis,
                Token::EOS,
            ]
        );
    }

    #[test]
    fn tokens_with_spaces() {
        assert_eq!(
            tokenize("   3  *    (5        + 2)    ").unwrap(),
            [
                Token::Literal(3.0),
                Token::Product,
                Token::OpeningParenthesis,
                Token::Literal(5.0),
                Token::Addition,
                Token::Literal(2.0),
                Token::ClosingParenthesis,
                Token::EOS
            ]
        );
    }

    #[test]
    fn tokens_with_points() {
        assert_eq!(
            tokenize("3.0 + 4").unwrap(),
            [
                Token::Literal(3.0),
                Token::Addition,
                Token::Literal(4.0),
                Token::EOS
            ]
        );
    }

    #[test]
    #[should_panic]
    fn invalid_tokens1() {
        tokenize("3 + 4 * {3}").unwrap();
    }

    #[test]
    #[should_panic]
    fn invalid_tokens2() {
        init_logger();
        let toks = tokenize("4+3.").unwrap();
        debug!("Tokens: {:?}", toks);
    }

    #[test]
    #[should_panic]
    fn invalid_tokens3() {
        tokenize(".").unwrap();
    }
}
