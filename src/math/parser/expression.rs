//! Submodule expression is part of parser module.
//!
//! An expression is formed of symbols which, put together, describes a mathematical operation.
//! Such an operation can be a formula to be evaluated, declaration of some variable, function...
//!
//! This submodule provides tools for describing such expressions in a convenient way for a
//! program.

use std::collections::HashMap;

const ZERO_THRESHOLD: f32 = 1e-10;

pub type VarTab = HashMap<String, f32>;
pub type FuncTab = HashMap<String, fn(Vec<f32>) -> f32>;

pub enum UnOp {
    Negation,
}

pub enum BinOp {
    Addition,
    Substraction,
    Product,
    Division,
}

/// The enum Expression describes a mathematical expression.
/// It is implemented recursively as a tree of expressions.
///
/// An Expression, after hving been built, perform following operations :
///  - evaluate given certain variables and functions.
///    Note that evaluating may result in an error in case variables/functions needed are not
///    provided.
pub enum Expression {
    NonEvaluable,
    Literal(f32),
    UnOp(UnOp, Box<Expression>),
    BinOp(BinOp, Box<Expression>, Box<Expression>),
    //TODO : implement variables and functions
    Variable(String),
    Assignment(String, Box<Expression>),
}

/// During evaluation, an error can occur for the following reasons :
///  - a variable was not found in the given map of variables
///  - same for function
///  - the expression has a non evaluable part
///  - division by zero
#[derive(Debug)]
pub enum EvaluationError {
    VariableNotFound(String),
    FunctionNotFound(String),
    NotEvaluable(String),
    DivisionByZero(String),
}

impl Expression {
    pub fn nonevaluable() -> Self {
        Expression::NonEvaluable
    }

    pub fn literal(n: f32) -> Self {
        Expression::Literal(n)
    }

    pub fn un_op(op: UnOp, e: Expression) -> Self {
        Expression::UnOp(op, Box::new(e))
    }

    pub fn bin_op(op: BinOp, lhs: Expression, rhs: Expression) -> Self {
        Expression::BinOp(op, Box::new(lhs), Box::new(rhs))
    }

    pub fn variable(name: String) -> Self {
        Expression::Variable(name)
    }

    pub fn assignment(name: String, expr: Expression) -> Self {
        Expression::Assignment(name, Box::new(expr))
    }
}

/// An expression can be recusively written to a string
impl ToString for Expression {
    fn to_string(&self) -> String {
        match self {
            Expression::NonEvaluable => String::from("NaN"),
            Expression::Literal(n) => n.to_string(),
            Expression::UnOp(op, e) => match op {
                UnOp::Negation => format!("-{}", e.to_string()),
            },
            Expression::BinOp(op, lhs, rhs) => {
                let (lhs, rhs) = (lhs.to_string(), rhs.to_string());
                match op {
                    BinOp::Addition => format!("({}+{})", lhs, rhs),
                    BinOp::Substraction => format!("({}-{})", lhs, rhs),
                    BinOp::Product => format!("({}*{})", lhs, rhs),
                    BinOp::Division => format!("({}/{})", lhs, rhs),
                }
            }
            Expression::Variable(name) => name.clone(),
            Expression::Assignment(name, expr) => format!("({}={})", name, expr.to_string()),
        }
    }
}

impl Expression {
    /// eval evaluates recursively a mathematical expression given a map of variables and functions
    pub fn eval(&self, vars: &mut VarTab, _funcs: &mut FuncTab) -> Result<f32, EvaluationError> {
        //TODO implement evaluation of variables and functions
        match self {
            Expression::NonEvaluable => Err(EvaluationError::NotEvaluable(self.to_string())),
            Expression::Literal(n) => Ok(*n),
            Expression::UnOp(op, e) => {
                let e = e.eval(vars, _funcs)?;
                match op {
                    UnOp::Negation => Ok(-e),
                }
            }
            Expression::BinOp(op, lhs, rhs) => {
                let (lhs_eval, rhs_eval) = (lhs.eval(vars, _funcs)?, rhs.eval(vars, _funcs)?);
                match op {
                    BinOp::Addition => Ok(lhs_eval + rhs_eval),
                    BinOp::Substraction => Ok(lhs_eval - rhs_eval),
                    BinOp::Product => Ok(lhs_eval * rhs_eval),
                    BinOp::Division => {
                        if rhs_eval.abs() < ZERO_THRESHOLD {
                            Err(EvaluationError::DivisionByZero(self.to_string()))
                        } else {
                            Ok(lhs_eval / rhs_eval)
                        }
                    }
                }
            }
            Expression::Variable(name) => {
                if let Some(&n) = vars.get(name) {
                    Ok(n)
                } else {
                    Err(EvaluationError::VariableNotFound(name.clone()))
                }
            }
            Expression::Assignment(name, expr) => {
                let val = expr.eval(vars, _funcs)?;
                vars.insert(name.clone(), val);
                Ok(val)
            }
        }
    }
}

#[cfg(test)]
mod evaluation_tests {
    use super::*;

    ///Test simple operations

    #[test]
    fn simple_additions() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let (lhs, rhs) = (Expression::literal(3.0), Expression::literal(4.0));
        let e = Expression::bin_op(BinOp::Addition, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), 7.0);

        let (lhs, rhs) = (Expression::literal(-3.0), Expression::literal(4.0));
        let e = Expression::bin_op(BinOp::Addition, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), 1.0);

        let (lhs, rhs) = (Expression::literal(0.0), Expression::literal(1.0));
        let e = Expression::bin_op(BinOp::Addition, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), 1.0);
    }

    #[test]
    fn simple_substractions() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let (lhs, rhs) = (Expression::literal(3.0), Expression::literal(4.0));
        let e = Expression::bin_op(BinOp::Substraction, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), -1.0);

        let (lhs, rhs) = (Expression::literal(-3.0), Expression::literal(4.0));
        let e = Expression::bin_op(BinOp::Substraction, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), -7.0);

        let (lhs, rhs) = (Expression::literal(0.0), Expression::literal(1.0));
        let e = Expression::bin_op(BinOp::Substraction, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), -1.0);
    }

    #[test]
    fn simple_products() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let (lhs, rhs) = (Expression::literal(3.0), Expression::literal(4.0));
        let e = Expression::bin_op(BinOp::Product, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), 12.0);

        let (lhs, rhs) = (Expression::literal(-3.0), Expression::literal(4.0));
        let e = Expression::bin_op(BinOp::Product, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), -12.0);

        let (lhs, rhs) = (Expression::literal(0.0), Expression::literal(1.0));
        let e = Expression::bin_op(BinOp::Product, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), 0.0);
    }

    #[test]
    fn simple_divisions() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let (lhs, rhs) = (Expression::literal(4.0), Expression::literal(2.0));
        let e = Expression::bin_op(BinOp::Division, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), 2.0);

        let (lhs, rhs) = (Expression::literal(-3.0), Expression::literal(0.1));
        let e = Expression::bin_op(BinOp::Division, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), -30.0);

        let (lhs, rhs) = (Expression::literal(0.0), Expression::literal(1.0));
        let e = Expression::bin_op(BinOp::Division, lhs, rhs);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), 0.0);
    }

    #[test]
    #[should_panic]
    fn division_by_zero() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let (lhs, rhs) = (Expression::literal(1.0), Expression::literal(0.0));
        let e = Expression::bin_op(BinOp::Division, lhs, rhs);
        e.eval(&mut vars, &mut funcs).unwrap();
    }

    #[test]
    #[should_panic]
    fn nonevaluable() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let e = Expression::nonevaluable();
        e.eval(&mut vars, &mut funcs).unwrap();
    }

    //TODO : implement tests for variables, functions
    /// Test for variables
    #[test]
    fn test_variables() {
        let mut vars = VarTab::new();
        vars.insert("x".to_string(), 3.0);
        let mut funcs = FuncTab::new();
        let x = Expression::variable("x".to_string());
        assert_eq!(x.eval(&mut vars, &mut funcs).unwrap(), 3.0);

        vars.insert("y".to_string(), 4.0);
        let y = Expression::variable("y".to_string());
        let c = Expression::literal(2.0);
        let e = Expression::bin_op(BinOp::Product, x, y);
        let e = Expression::bin_op(BinOp::Substraction, e, c);
        assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), 10.0);
    }

    #[test]
    #[should_panic]
    fn test_variable_panic() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let void = Expression::variable("x".to_string());
        assert_eq!(void.eval(&mut vars, &mut funcs).unwrap(), 1.0);
    }
}

#[cfg(test)]
mod tostring_tests {
    use super::*;

    #[test]
    fn literal_to_string() {
        let e = Expression::literal(3.1);
        assert_eq!(e.to_string(), "3.1");
    }

    #[test]
    fn un_op_to_string() {
        let e = Expression::literal(3.1);
        let e = Expression::un_op(UnOp::Negation, e);
        assert_eq!(e.to_string(), "-3.1");
    }

    #[test]
    fn bin_op_to_string() {
        let (lhs, rhs) = (Expression::literal(1.1), Expression::literal(2.1));
        let e = Expression::bin_op(BinOp::Addition, lhs, rhs);
        assert_eq!(e.to_string(), "(1.1+2.1)");

        let (lhs, rhs) = (Expression::literal(1.1), Expression::literal(2.1));
        let e = Expression::bin_op(BinOp::Substraction, lhs, rhs);
        assert_eq!(e.to_string(), "(1.1-2.1)");

        let (lhs, rhs) = (Expression::literal(1.1), Expression::literal(2.1));
        let e = Expression::bin_op(BinOp::Product, lhs, rhs);
        assert_eq!(e.to_string(), "(1.1*2.1)");

        let (lhs, rhs) = (Expression::literal(1.1), Expression::literal(2.1));
        let e = Expression::bin_op(BinOp::Division, lhs, rhs);
        assert_eq!(e.to_string(), "(1.1/2.1)");
    }

    #[test]
    fn nonevaluable_to_string() {
        let e = Expression::nonevaluable();
        assert_eq!(e.to_string(), "NaN");
    }
}
