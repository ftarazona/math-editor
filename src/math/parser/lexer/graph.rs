//! Submodule graph is part of lexer submodule
//!
//! The lexer uses a graph to recognize tokens in a given expression
//!
//! This submodule provides tools for handling graph traversal in this context.

/// A transition represents a link in the graph.
pub struct Transition {
    pub src_state: u32,
    pub dst_state: u32,
    pub pred: fn(char) -> bool,
}

/// When traversing the graph the following erros can occur :
///  - End of string was reached before reaching an accepting state
///  - A given token is not recognized
pub enum GraphError {
    ReachedEOS,
    Unaccepted,
}

/// Given an expression, determines the first token to be found from the left.
/// Parameters are :
///  - s: the expression to analyse,
///  - current_state: the initial state when called from outside
///  - transitions: an array indicating the transitions of the graph
///  - accepting: an array of the accepting states
/// On success, returns:
///  - the final state
///  - the expression which satisfies it
///  - the expression remaining from the initial one to be analyzed
pub fn accept<'a>(
    s: &'a str,
    current_state: u32,
    transitions: &[Transition],
    accepting: &[u32],
) -> Result<(u32, String, &'a str), GraphError> {
    if let Some(c) = s.chars().next() {
        if let Ok(next_state) = find_next_state(current_state, c, transitions) {
            let (final_state, mut string, remaining) =
                accept(&s[1..], next_state, transitions, accepting)?;
            string.insert(0, c);
            Ok((final_state, string, remaining))
        } else if accepting.contains(&current_state) {
            Ok((current_state, String::new(), s))
        } else {
            Err(GraphError::Unaccepted)
        }
    } else {
        if accepting.contains(&current_state) {
            Ok((current_state, String::new(), s))
        } else {
            Err(GraphError::ReachedEOS)
        }
    }
}

/// Determines next state given a current state and a character of transition.
/// Note that the graph is supposed to be deterministic, so that AT MOST ONE transition can be
/// satisfied
fn find_next_state(
    current_state: u32,
    current_char: char,
    transitions: &[Transition],
) -> Result<u32, GraphError> {
    for transition in transitions.iter() {
        if transition.src_state == current_state && (transition.pred)(current_char) {
            return Ok(transition.dst_state);
        }
    }
    Err(GraphError::Unaccepted)
}
