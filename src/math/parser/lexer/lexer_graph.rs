use super::graph::Transition;

pub const GRAPH: [Transition; 14] = [
    Transition {
        src_state: 0,
        dst_state: 0,
        pred: |c| c.is_whitespace(),
    },
    Transition {
        src_state: 0,
        dst_state: 1,
        pred: |c| c == '+',
    },
    Transition {
        src_state: 0,
        dst_state: 2,
        pred: |c| c == '-',
    },
    Transition {
        src_state: 0,
        dst_state: 3,
        pred: |c| c == '*',
    },
    Transition {
        src_state: 0,
        dst_state: 4,
        pred: |c| c == '/',
    },
    Transition {
        src_state: 0,
        dst_state: 5,
        pred: |c| c.is_alphabetic(),
    },
    Transition {
        src_state: 5,
        dst_state: 5,
        pred: |c| c.is_alphanumeric() || c == '_',
    },
    Transition {
        src_state: 0,
        dst_state: 6,
        pred: |c| c == '(',
    },
    Transition {
        src_state: 0,
        dst_state: 7,
        pred: |c| c == ')',
    },
    Transition {
        src_state: 0,
        dst_state: 8,
        pred: |c| c.is_numeric(),
    },
    Transition {
        src_state: 8,
        dst_state: 8,
        pred: |c| c.is_numeric(),
    },
    Transition {
        src_state: 8,
        dst_state: 9,
        pred: |c| c == '.',
    },
    Transition {
        src_state: 9,
        dst_state: 8,
        pred: |c| c.is_numeric(),
    },
    Transition {
        src_state: 0,
        dst_state: 10,
        pred: |c| c == '=',
    },
];

pub const ACCEPTING: [u32; 9] = [1, 2, 3, 4, 5, 6, 7, 8, 10];
