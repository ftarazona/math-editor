//! Submode expression_parser is part of parser submodule
//!
//! The parser is a recursive descent one, allowing operation precedence.
//!
//! This submodule provides auxiliary functions to produce different expressions.
//! It is strongly inspried by the LLVM tutorial. This can be found here :
//!  https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/LangImpl02.html

use log::debug;

use super::expression::{BinOp, Expression, UnOp};
use super::lexer::Token;

#[derive(Debug)]
pub enum ParserError {
    ExpectedLiteral,
    ExpectedVariable,
    ExpectedFunction,
    ExpectedClosingParenthesis,
    UnexpectedToken,
    ExpectedExpression,
    UnexpectedAssignment,
    TokenizationError,
    NoExpressionFound,
}

///Definition of a PeekableIterator trait for convenience
///A peekable iterator allows us to foresee without consuming.
pub trait PeekableIterator: std::iter::Iterator {
    fn peek(&mut self) -> Option<&Self::Item>;
}

impl<I: std::iter::Iterator> PeekableIterator for std::iter::Peekable<I> {
    fn peek(&mut self) -> Option<&Self::Item> {
        std::iter::Peekable::peek(self)
    }
}

///Definition of operators precedence.
///Additions and substractions are less prior thant products and divisions.
fn token_precedence(tok: Option<&&Token>) -> i32 {
    match tok {
        Some(Token::Addition) => 20,
        Some(Token::Substraction) => 20,
        Some(Token::Product) => 40,
        Some(Token::Division) => 40,
        _ => -1,
    }
}

///Given a list of tokens in the form of a peekable iterator,
///returns the corresponding expression. If no expression was actually given, returns None.
///Returns a ParserError in case an issue occurs.
pub fn parse_expression<'a>(
    toks: &mut impl PeekableIterator<Item = &'a Token>,
) -> Result<Option<Expression>, ParserError> {
    let lhs = parse_primary_expression(toks)?;
    if let Some(lhs) = lhs {
        if let Some(&&Token::Assignment) = toks.peek() {
            toks.next();
            if let Expression::Variable(name) = lhs {
                if let Some(expr) = parse_expression(toks)? {
                    Ok(Some(Expression::assignment(name, expr)))
                } else {
                    Err(ParserError::ExpectedExpression)
                }
            } else {
                Err(ParserError::ExpectedVariable)
            }
        } else {
            parse_bin_op_rhs(toks, 0, lhs)
        }
    } else {
        Ok(None)
    }
}

///Determines the rhs of a binary operation, taking into account the precedence of operators.
fn parse_bin_op_rhs<'a>(
    toks: &mut impl PeekableIterator<Item = &'a Token>,
    expr_prec: i32,
    mut lhs: Expression,
) -> Result<Option<Expression>, ParserError> {
    loop {
        let bin_op = toks.peek();
        if let Some(&&Token::Assignment) = bin_op {
            break Err(ParserError::UnexpectedAssignment);
        }
        let bin_op_prec = token_precedence(bin_op);
        if bin_op_prec < expr_prec {
            break Ok(Some(lhs));
        }
        debug!("DETECTED BINARY OEPRATION");
        let bin_op = toks.next();

        let rhs = parse_primary_expression(toks)?;
        if let None = rhs {
            break Ok(None);
        }
        let mut rhs = rhs.unwrap();

        let next_bin_op = toks.peek();
        let next_bin_op_prec = token_precedence(next_bin_op);

        if bin_op_prec < next_bin_op_prec {
            if let Some(new_rhs) = parse_bin_op_rhs(toks, bin_op_prec + 1, rhs)? {
                rhs = new_rhs;
            } else {
                break Ok(None);
            }
        }

        match bin_op {
            Some(Token::Addition) => lhs = Expression::bin_op(BinOp::Addition, lhs, rhs),
            Some(Token::Substraction) => lhs = Expression::bin_op(BinOp::Substraction, lhs, rhs),
            Some(Token::Product) => lhs = Expression::bin_op(BinOp::Product, lhs, rhs),
            Some(Token::Division) => lhs = Expression::bin_op(BinOp::Division, lhs, rhs),
            _ => break Err(ParserError::ExpectedExpression),
        }
    }
}

///Parses a primary expression.
///A primary expression is either a literal, a variable, a parenthesized expression, an expression
///preceded by a sign indicator.
fn parse_primary_expression<'a>(
    toks: &mut impl PeekableIterator<Item = &'a Token>,
) -> Result<Option<Expression>, ParserError> {
    match toks.next() {
        Some(Token::OpeningParenthesis) => parse_parenthesis(toks),
        Some(Token::Addition) => parse_primary_expression(toks),
        Some(Token::Literal(n)) => Ok(Some(Expression::literal(*n))),
        Some(Token::SymbolName(name)) => Ok(Some(Expression::variable(name.clone()))),
        Some(Token::Substraction) => {
            if let Some(expr) = parse_primary_expression(toks)? {
                Ok(Some(Expression::un_op(UnOp::Negation, expr)))
            } else {
                Err(ParserError::ExpectedExpression)
            }
        }
        _ => Ok(None),
    }
}

///Parses a parenthesized expression.
fn parse_parenthesis<'a>(
    toks: &mut impl PeekableIterator<Item = &'a Token>,
) -> Result<Option<Expression>, ParserError> {
    let expr = parse_expression(toks)?;
    let tok = toks.next();
    debug!("Token read for closing parenthesis : {:?}", tok);
    if let Some(Token::ClosingParenthesis) = tok {
        Ok(expr)
    } else {
        Err(ParserError::ExpectedClosingParenthesis)
    }
}

#[cfg(test)]
mod tests {
    use super::super::expression::{FuncTab, VarTab};
    use super::super::lexer::tokenize;
    use super::*;
    use log::debug;

    fn init_logger() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn parser_no_bin_op() {
        init_logger();
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let expressions = [
            ("3.0", 3.0),
            ("-2", -2.0),
            ("(9)", 9.0),
            ("(-8)", -8.0),
            ("+4.1", 4.1),
            ("(+2)", 2.0),
        ];
        for (expr_str, expr_res) in expressions.iter() {
            debug!(
                "Expression test : {}. Should result in : {}",
                expr_str, expr_res
            );
            debug!("Tokens: {:?}", tokenize(expr_str).unwrap());
            let e = parse_expression(&mut tokenize(expr_str).unwrap().iter().peekable())
                .unwrap()
                .unwrap();
            assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), *expr_res);
        }
    }

    #[test]
    fn parser_one_bin_op() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let expressions = [
            ("3+4", 7.0),
            ("-2+2", 0.0),
            ("(3-9)", -6.0),
            ("(-8 * 7)", -56.0),
            ("4/2.0", 2.0),
            ("(+2+3)", 5.0),
        ];
        for (expr_str, expr_res) in expressions.iter() {
            debug!(
                "Expression test : {}. Should result in : {}",
                expr_str, expr_res
            );
            debug!("Tokens: {:?}", tokenize(expr_str).unwrap());
            let e = parse_expression(&mut tokenize(expr_str).unwrap().iter().peekable())
                .unwrap()
                .unwrap();
            assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), *expr_res);
        }
    }

    #[test]
    fn parser_two_bin_op() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let expressions = [
            ("3+4*8", 35.0),
            ("-2+2/2", -1.0),
            ("5 * (3-9)", -30.0),
            ("(-8 * 7)", -56.0),
            ("4/2.0/2", 1.0),
            ("10/(+2+3)", 2.0),
        ];
        for (expr_str, expr_res) in expressions.iter() {
            debug!(
                "Expression test : {}. Should result in : {}",
                expr_str, expr_res
            );
            debug!("Tokens: {:?}", tokenize(expr_str).unwrap());
            let e = parse_expression(&mut tokenize(expr_str).unwrap().iter().peekable())
                .unwrap()
                .unwrap();
            assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), *expr_res);
        }
    }

    #[test]
    fn parser_complex_expressions() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let expressions = [("3+4*8+2/2", 36.0), ("-2+2/(6*(3-1)-13)", -4.0)];
        for (expr_str, expr_res) in expressions.iter() {
            debug!(
                "Expression test : {}. Should result in : {}",
                expr_str, expr_res
            );
            debug!("Tokens: {:?}", tokenize(expr_str).unwrap());
            let e = parse_expression(&mut tokenize(expr_str).unwrap().iter().peekable())
                .unwrap()
                .unwrap();
            assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), *expr_res);
        }
    }

    #[test]
    fn parser_variables() {
        let mut vars = VarTab::new();
        vars.insert("x".to_string(), 3.0);
        let mut funcs = FuncTab::new();
        let expressions = [("x * x", 9.0), ("3.0 + (-x) / 3.0", 2.0)];
        for (expr_str, expr_res) in expressions.iter() {
            debug!(
                "Expression test : {}. Should result in : {}",
                expr_str, expr_res
            );
            debug!("Tokens: {:?}", tokenize(expr_str).unwrap());
            let e = parse_expression(&mut tokenize(expr_str).unwrap().iter().peekable())
                .unwrap()
                .unwrap();
            assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), *expr_res);
        }
    }

    #[test]
    fn parser_assignments() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let expressions = [("x = 3.0", 3.0), ("y = x * x + 2.0", 11.0), ("x + y", 14.0)];
        for (expr_str, expr_res) in expressions.iter() {
            debug!(
                "Expression test : {}. Should result in : {}",
                expr_str, expr_res
            );
            debug!("Tokens: {:?}", tokenize(expr_str).unwrap());
            let e = parse_expression(&mut tokenize(expr_str).unwrap().iter().peekable())
                .unwrap()
                .unwrap();
            assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), *expr_res);
        }
    }

    #[test]
    #[should_panic]
    fn parser_panic3() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let expressions = [("3.0 * 2.0)", 9.0)];
        for (expr_str, expr_res) in expressions.iter() {
            debug!(
                "Expression test : {}. Should result in : {}",
                expr_str, expr_res
            );
            debug!("Tokens: {:?}", tokenize(expr_str).unwrap());
            let e = parse_expression(&mut tokenize(expr_str).unwrap().iter().peekable())
                .unwrap()
                .unwrap();
            assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), *expr_res);
        }
    }

    #[test]
    #[should_panic]
    fn parser_panic2() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let expressions = [("(9.0 + 8.0", 17.0)];
        for (expr_str, expr_res) in expressions.iter() {
            debug!(
                "Expression test : {}. Should result in : {}",
                expr_str, expr_res
            );
            debug!("Tokens: {:?}", tokenize(expr_str).unwrap());
            let e = parse_expression(&mut tokenize(expr_str).unwrap().iter().peekable())
                .unwrap()
                .unwrap();
            assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), *expr_res);
        }
    }

    #[test]
    #[should_panic]
    fn parser_panic() {
        let mut vars = VarTab::new();
        let mut funcs = FuncTab::new();
        let expressions = [("2.0 * ", 9.0)];
        for (expr_str, expr_res) in expressions.iter() {
            debug!(
                "Expression test : {}. Should result in : {}",
                expr_str, expr_res
            );
            debug!("Tokens: {:?}", tokenize(expr_str).unwrap());
            let e = parse_expression(&mut tokenize(expr_str).unwrap().iter().peekable())
                .unwrap()
                .unwrap();
            assert_eq!(e.eval(&mut vars, &mut funcs).unwrap(), *expr_res);
        }
    }
}
